package com.neon.cdi;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class Main {
	public static void main(String[] args) {
		Weld weld = new Weld();
		WeldContainer container = weld.initialize();

		NumberPrinter np = container.select(NumberPrinter.class).get();

		for (int i = 0; i < 10; i++) {
			np.printNumber();
		}
	}
}
