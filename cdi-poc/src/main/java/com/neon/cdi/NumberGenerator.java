package com.neon.cdi;

public interface NumberGenerator {
    int generate();
}
