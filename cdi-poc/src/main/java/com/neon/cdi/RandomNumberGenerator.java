package com.neon.cdi;

import java.util.Random;

@com.neon.cdi.Random
public class RandomNumberGenerator implements NumberGenerator {

	@Override
	public int generate() {
		return Math.abs(new Random().nextInt());
	}

}
