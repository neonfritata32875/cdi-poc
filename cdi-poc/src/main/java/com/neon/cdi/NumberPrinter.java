package com.neon.cdi;

import javax.inject.Inject;

public class NumberPrinter {

	@Inject
	@Random
	private NumberGenerator generator;

	public void printNumber() {
		System.out.println(generator.generate());
	}
}
